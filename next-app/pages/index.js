import Link from 'next/link'
import {MainLayout} from "../components/mainLayout";


export default function Index() {
    return  (
        <MainLayout title='Home page'>
            <h1>hello next.js!</h1>
            <Link href={'/about'}><a>About</a></Link>
            <Link href={'/posts'}><a>Posts</a></Link>
        </MainLayout>
    )
}