import {useState, useEffect} from 'react'
import Link from "next/link";
import {MainLayout} from "../../components/mainLayout";
import {useRouter} from "next/router";

export default function Post({post: serverPost})  {
    console.log('serverPost: ', serverPost)
    const [ post, setPost ] = useState(serverPost)
    const router = useRouter()
    console.log('post: ', post)

    useEffect(()=> {
        async function load() {
            const response = await fetch(`http://localhost:4200/posts/${router.query.id}`)
            const data = await response.json()
            setPost(data)
        }

        if (!post) {
            load()
        }
    }, [])

    if (!post) {
        return <MainLayout>
            <p>Loading...</p>
        </MainLayout>
    }

    return (
        <MainLayout>
            <h1>{post.id}: {post.title}</h1>
            <button><Link href='/posts'><a>Обратно</a></Link></button>
        </MainLayout>
    )
}

Post.getInitialProps = async ({query, req}) => {
    if ( !req ) {
        return {post: null}
    }
    const response = await fetch(`http://localhost:4200/posts/${query.id}`)
    const post = await response.json()

    return {
        post
    }
}