import '../styles/main.scss'
import NextNprogress from 'nextjs-progressbar';

export default function MyApp({ Component, pageProps }) {
    return (
        <>
            <Component {...pageProps} />
            <NextNprogress
                color="yellow"
                startPosition="0.3"
                stopDelayMs="200"
                height="3"
            />
        </>
    )
}