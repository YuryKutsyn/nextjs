import Router from "next/router";
import {MainLayout} from "../../components/mainLayout";

export default function Index({ title }) {

    const linkClickHandler = () => {
        Router.push('/')
    }

    return (
        <MainLayout title='About page'>
                <h1>{title}</h1>

                <button onClick={()=> linkClickHandler()}>Go back to home</button>
                <button onClick={()=> Router.push('/posts')}>Go back to posts</button>
        </MainLayout>
    )
}

Index.getInitialProps = async () => {
    const response = await fetch('http://localhost:4200/about')
    const data = await response.json()

    return {
        title: data.title
    }
}